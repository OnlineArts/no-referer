<?php

$accept_language = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);
if(isset($_GET['goal'])) {
	$goal_tmp = $_GET['goal'];
	unset($_GET['goal']);
	
	# Block Liste #
	$blocking_goals = '/iphone4up/';
	if(preg_match($blocking_goals,$goal_tmp)) {
		die('malpractice detected. User information saved.');
	}
		
	if(preg_match('|http://|',$goal_tmp)) {
		$goal = urldecode($goal_tmp);
	} elseif(preg_match('|http:/|',$goal_tmp)) {
		$goal = substr($goal_tmp,6);
		$goal = urldecode('http://'.$goal);
	} elseif(preg_match('|https:/|',$goal_tmp)) {
		$goal = substr($goal_tmp,7);
		$goal = urldecode('https://'.$goal);
	} else {
		$goal = 'http://'.urldecode($goal_tmp);
	}
	header('Location: '.$goal);
} else {
$lang = 'en';
if(isset($accept_language)) {
	$lang = $accept_language;
}

# Languages #
if($lang == 'en' OR $lang == 'us') {
	$text =  'The <strong>referrer</strong> [...] identifies, from the point of view of an Internet webpage or resource, the address of the webpage [...] of the resource which links to it. By checking the referrer, the new webpage can see where the request originated.';
	$link = 'Example: ';
} elseif($lang = 'de') {
	$text =  'Ein <strong>Referrer</strong> ist die Internetadresse der Webseite, von der der Benutzer durch Anklicken eines Links zu der aktuellen Seite gekommen ist (engl. to refer „verweisen“). Der Referrer ist ein optionaler Teil der an den Webserver geschickten HTTP-Anfrage.';
	$link = 'Beispiel: ';
} elseif($lang = 'fr') {
	$text =  "Un référent ou référant, plus connu sous l'anglicisme <strong>referer</strong> ou referrer, est, dans le domaine des réseaux informatiques, une information transmise à un serveur HTTP lorsqu'un visiteur suit un lien pour accéder à l'une de ses ressources, lui indiquant l'URL de la page où se situe ce lien qu'il a suivi.<br />Cela peut être un lien sur une page extérieure au site, mais également un lien sur le même site.<br />Si aucun lien n'est fourni, le champ référant sera vide.";
	$link = 'Exemple : ';
} else {
	$text =  'The <strong>referrer</strong> [...] identifies, from the point of view of an Internet webpage or resource, the address of the webpage [...] of the resource which links to it. By checking the referrer, the new webpage can see where the request originated.';
	$link = 'Example: ';
}
#

echo '<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
     "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="'.$lang.'" lang="'.$lang.'" >
<head>
<title>No-Referrer</title>
<meta http-equiv="content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="content-language" content="'.$lang.'" />
	<meta name="author" content="Roman Martin" />
	<meta name="publisher" content="Online Arts" />
	<meta name="copyright" content="© 2011 Online Arts" />
	<meta name="keywords" content="Dereferrer, no referrer" />
	<meta name="description" content="A Dereferrer Service" />
	<meta name="date" content="2011-05-27T19:03:00+01:00" />
	<meta name="revisit-after" content="1 month" />
	<meta name="page-topic" content="Dereferrer, Anonymity" />
	<meta name="audience" content="Everybody" />
</head>
<body>
<center>
<div id="placeholder"></div>
<h1>No-Refer(r)er</h1>
<form method="get" action="#">
<div id="input">
	<input type="text" name="goal" size="40" class="input" value="http://www.google.com" class="button">
        <input type="submit">Go</input>
</div>
</form>
<br />
<div id="box">
<p>'.$link.' <a href="http://no-referer.de/http://www.google.com">http://no-referer.de/<u>http://www.google.com/</u></a></p>
<br />
<cite>'.$text.'</cite><br /><br />
<p>Wikipedia.org</p>
</div>
</center>
</body>
</html>';
}
?>

